const ww = require("./lib/ww");
const winston = require("winston");
const util = require("util")

// Handle all "actionSelected" annotation types
module.exports.selected = function (body, token, url) {
  winston.log("debug", "Entered action.selected");
  winston.log("debug", "body" + JSON.stringify(body));
  var payload = JSON.parse(body.annotationPayload);

  // New interaction?
  if (payload.actionId.includes("new")) {
    handleNewDialog(body, token, url);
  }
  // or response?
  else {
    handleDialogResponse(body, token, url);
  }
}

// Any new user click on an underlined item in a space goes here.
var handleNewDialog = function (body, token, url) {
  winston.log("debug", "Entered action.handleNewDialog");
  var payload = JSON.parse(body.annotationPayload);
  var action = payload.actionId;
  if (action.includes("reply_to_ticket")) {
    var buttons = [];
    buttons.push(createButton("Private", "PRIVATE_REPLY", "PRIMARY"));
    buttons.push(createButton("Public", "PUBLIC_REPLY", "PRIMARY"));
    for (var i = 0; i < 20; i++) {
      buttons.push(createButton(i, i, "SECONDARY");
    }
    var dialog = createDialogText("Write a reply to ticket",
      "Should this reply be public or private?",
      buttons.join(','));
    sendResponse(dialog, body, token, url);
  }
}

// Any interaction from the user with the dialog goes here
var handleDialogResponse = function (body, token, url) {
  winston.log("debug", "Entered action.handleDialogResponse");
  var payload = JSON.parse(body.annotationPayload);
  var action = payload.actionId;
  if (action.includes("PRIVATE_REPLY")) {
    var dialog = createDialogText("Write a reply to ticket",
      "I have added a private note questioning the ancestry of your FLM",
      "");
    sendResponse(dialog, body, token, url);
  }
  else if (action.includes("PUBLIC_REPLY")) {
    var dialog = createDialogText("Write a reply to ticket",
      "I have replied to the customer with a random NSFW quote",
      "");
    sendResponse(dialog, body, token, url);
  }
}
// Make the graphQL call with the mutation
var sendResponse = function (dialog, body, token, url) {
  winston.log("debug", "Entered action.sendResponse");
  var payload = JSON.parse(body.annotationPayload);
  var conversation_id = payload.conversationId;
  var target_user_id = body.userId;
  var target_dialog_id = payload.targetDialogId;
  var view = "PUBLIC, BETA"
  var mutation_body = util.format(`
    mutation {
      createTargetedMessage(input: {
        conversationId: "%s"
        targetUserId: "%s"
        targetDialogId: "%s"
        annotations: [ %s ]
      }) {
        successful
      }
    }`, conversation_id, target_user_id, target_dialog_id, dialog);
  ww.makeGraphQLCall(mutation_body, view, token, url, function (err, res) {
    if (err) {
      winston.log("error", "mutation graphql call failed", err);
    } else {
      winston.log("debug", "mutation graphql call succeeded", res);
    }
  });
}

var createButton = function (label, id, style) {
  winston.log("debug", "Entered action.createButton");
  const button = util.format(`
    {
      postbackButton: {
        title: "%s",
        id: "%s",
        style: %s
      }
    }`, label, id, style);
  return button;
}

var createDialogText = function (title, description, buttons) {
  winston.log("debug", "Entered action.createDialogText");
  var color = "#016F4A";
  const annotation = util.format(`
    {
      genericAnnotation: {
        title: "%s",
        text: "%s",
        color: "%s",
        buttons: [ %s ]
      }
    }`, title, description, color, buttons);
  return annotation;
}
