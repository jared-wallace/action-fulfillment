const express = require("express");
const request = require("request");
const bodyParser = require("body-parser");
const fs = require("fs");
const https = require("https");
const winston = require("winston");
const ww = require("./lib/ww");
const action = require("./action");

const SPACE_ID = "58fb7e28e4b0180503eccc07";
const WW_URL = "https://api.watsonwork.ibm.com";

var APP_ID = process.env.APP_ID;
var APP_SECRET = process.env.APP_SECRET;
var WEBHOOK_SECRET = process.env.WEBHOOK_SECRET;
var PORT = process.env.PORT;
var privateKey = fs.readFileSync("key.pem");
var certificate = fs.readFileSync("cert.pem");
var app = express();
var token = {};

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, {'timestamp':true});
winston.level = "debug";


// Callback from Watson Workspace.
app.post("/ww", function (req, res) {
  winston.log("info", "Received POST to /ww");
  var body = req.body;
  var eventType = body.type;
  var annotationType = body.annotationType;
  winston.log("debug", "Here's what we got:", body);

  // Verification event
  if (eventType === "verification") {
    winston.log("debug", "Verifying...");
    ww.verifyWorkspace(res, body.challenge, WEBHOOK_SECRET);
    return;
  }
  res.status(200).end();
  // Message created event
  if (eventType === "message-created") {
    // Ignore our own messages
    if (body.userId === APP_ID) {
      return;
    }
    var text = body.content.toLowerCase();
    // Handle if we were mentioned
    if (text.includes('@box')) {
      winston.log("info", "We were mentioned in a message");
      winston.log("debug", "Message was: " + text);
      boxbot.handleMessage(body, token, WW_URL, SPACE_ID);
    }
  }
  else if (annotationType === "actionSelected") {
    winston.log("debug", "Received actionSelected");
    winston.log("debug", "Here's what we got:", body);
    action.selected(body, token, WW_URL);
  }

});

https.createServer({
  key: privateKey,
  cert: certificate
}, app).listen(PORT, function (err, res) {
  winston.log("info", "Server started on port " + PORT);
});

// Grab our initial WW auth token. They're good for roughly 12 hours.
ww.getToken(WW_URL, APP_ID, APP_SECRET, function (err, res) {
  if (err) {
    winston.log("error", "Failed to obtain initial token", err);
  } else {
    token = res;
  }
});
